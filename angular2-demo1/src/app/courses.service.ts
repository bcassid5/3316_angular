import { Injectable } from '@angular/core';

@Injectable()
export class CoursesService {
  
  allCourses = ["3316 - Web Tech", "3313 - Operating Systems"];  
  
    
  getCourses() : string[] {
      return this.allCourses;
  }
  
  saveCourse(newEntry:string){
    this.allCourses.push(newEntry);
  }

}
