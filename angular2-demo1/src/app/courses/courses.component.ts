import { Component } from '@angular/core'
import {CoursesService} from '../courses.service'


@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css'],
  providers: [CoursesService]
})
export class CoursesComponent {
  
  cTitle = "My Current Courses";
  
  courses;
  courseInput;
  

  constructor(coursesService: CoursesService) {
    this.courses = coursesService.getCourses();
  }

  onClickMe() {
      this.courses.push(this.courseInput);
      //this.pushToService();
  }
  
  pushToService(coursesService?: CoursesService){
    coursesService.saveCourse(this.courseInput);
  }

}
